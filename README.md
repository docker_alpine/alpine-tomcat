# alpine-tomcat
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-tomcat)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-tomcat)



----------------------------------------
### x64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-tomcat/x64)
### aarch64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-tomcat/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [Tomcat](https://tomcat.apache.org/)
    - The Apache Tomcat® software is an open source implementation of the Java Servlet, JavaServer Pages, Java Expression Language and Java WebSocket technologies.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 8080:8080/tcp \
           forumi0721/alpine-tomcat:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:8080/](http://localhost:8080/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8080/tcp           | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

